#include<stdio.h>
#include<stdlib.h>
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions
#include "lst_elm.h"
#include "lst.h"

/***
	* ALGORITHME (FONCTION PRINCIPALE)
	***/
int main(){
	int v;
	struct lst_t * L = new_lst();
	scanf( "%d", &v );
	//cons(L,v);
	print_lst(L);
	
	while( v > 0 ) {
		//cons(L,v);
		//queue(L,v);
		insert_ordered(L,v);
		//print_lst(L);
		scanf("%d",&v);
	}
	print_lst(L);
	del_lst(&L);
	//print_lst(L);
	return EXIT_SUCCESS;
}