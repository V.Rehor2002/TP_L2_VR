#include "lst_elm.h"

#ifndef _LST_
#define _LST_

/** @brief Le type d'une liste :
	* + head - le premier élément de la liste
	* + tail - le dernier élément de la liste
	* + numelm - le nombre d'élément dans la liste
*/
struct lst_t {
	struct lst_elm_t * head;
	struct lst_elm_t * tail;
	int numelm;
};
/***
 * DÉCLARATION DES FONCTIONS PUBLIQUES
 ***/
/** @brief Construire une liste vide */
struct lst_t * new_lst();
/** @brief Libèrer la mémoire occupée par la liste */
void del_lst(struct lst_t ** ptrL );
/** @brief Vérifier si la liste L est vide ou pas */
bool empty_lst(const struct lst_t * L);
/** @brief Ajouter en tête de la liste L la valeur v */
void cons(struct lst_t * L, int v);
void queue(struct lst_t * L, int v);
/** @brief Visualiser les éléments de la liste L */
void print_lst(struct lst_t * L );

void insert_after(struct lst_t * L, const int value, struct lst_elm_t * place);

void insert_ordered(struct lst_t * L, const int value);

#endif//_LST_